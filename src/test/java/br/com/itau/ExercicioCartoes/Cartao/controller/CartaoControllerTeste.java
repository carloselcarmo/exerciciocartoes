package br.com.itau.ExercicioCartoes.Cartao.controller;

import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoEntrada;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoSaida;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoSemStatus;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoStatus;
import br.com.itau.ExercicioCartoes.Cartao.service.CartaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(CartaoController.class)
public class CartaoControllerTeste
{
    @MockBean
    private CartaoService cartaoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarCriarCartao() throws Exception
    {
        //Preparação
        CartaoEntrada cartaoEntradaDTO = new CartaoEntrada();
        cartaoEntradaDTO.setClienteId(1);
        cartaoEntradaDTO.setNumero("123");

        CartaoSaida cartaoSaidaDTO = new CartaoSaida();
        cartaoSaidaDTO.setAtivo(false);
        cartaoSaidaDTO.setClienteId(1);
        cartaoSaidaDTO.setId(1);
        cartaoSaidaDTO.setNumero("123");

        Mockito.when(cartaoService.criar(Mockito.any(CartaoEntrada.class))).thenReturn(cartaoSaidaDTO);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoJson = objectMapper.writeValueAsString(cartaoEntradaDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/cartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.numero", CoreMatchers.equalTo("123")));
    }

    @Test
    public void testarBuscarPorNumero() throws Exception
    {
        //Preparação
        CartaoEntrada cartaoEntradaDTO = new CartaoEntrada();
        cartaoEntradaDTO.setClienteId(1);
        cartaoEntradaDTO.setNumero("123");

        CartaoSemStatus cartaoSaidaDTO = new CartaoSemStatus();
        cartaoSaidaDTO.setClienteId(1);
        cartaoSaidaDTO.setId(1);
        cartaoSaidaDTO.setNumero("123");

        Mockito.when(cartaoService.buscarPorNumero(Mockito.anyString())).thenReturn(cartaoSaidaDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/cartao/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void TestarBuscarPorNumeroSemResposta() throws Exception
    {
        //Preparação
        Mockito.when(cartaoService.buscarPorNumero(Mockito.anyString())).thenThrow(RuntimeException.class);


        mockMvc.perform(MockMvcRequestBuilders.get("/cartao/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAlterarStatusAtivo() throws Exception
    {
        //Preparação
        CartaoStatus cartaoEntradaDTO = new CartaoStatus();
        cartaoEntradaDTO.setAtivo(true);

        CartaoSaida cartaoSaidaDTO = new CartaoSaida();
        cartaoSaidaDTO.setAtivo(true);
        cartaoSaidaDTO.setClienteId(1);
        cartaoSaidaDTO.setId(1);
        cartaoSaidaDTO.setNumero("123");

        Mockito.when(cartaoService.alterarStatusAtivo(Mockito.anyString(), Mockito.any(CartaoStatus.class))).thenReturn(cartaoSaidaDTO);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoJson = objectMapper.writeValueAsString(cartaoEntradaDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.patch("/cartao/1")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.ativo", CoreMatchers.equalTo(true)));
    }

    @Test
    public void TestarAlterarStatusAtivoComErro() throws Exception
    {
        //Preparação
        Mockito.when(cartaoService.alterarStatusAtivo(Mockito.anyString(), Mockito.any(CartaoStatus.class))).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.patch("/cartao/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
