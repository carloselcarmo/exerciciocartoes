package br.com.itau.ExercicioCartoes.Pagamento.service;

import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoStatusSaida;
import br.com.itau.ExercicioCartoes.Cartao.model.Cartao;
import br.com.itau.ExercicioCartoes.Cartao.service.CartaoService;
import br.com.itau.ExercicioCartoes.Cliente.model.Cliente;
import br.com.itau.ExercicioCartoes.Cliente.repository.ClienteRepository;
import br.com.itau.ExercicioCartoes.Cliente.service.ClienteService;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.PagamentoEntrada;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.PagamentoSaida;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.ValorPago;
import br.com.itau.ExercicioCartoes.Pagamento.model.Pagamento;
import br.com.itau.ExercicioCartoes.Pagamento.repository.PagamentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class PagamentoServiceTeste
{
    @MockBean
    private PagamentoRepository pagamentoRepository;

    @MockBean
    private ClienteRepository clienteRepository;

    @MockBean
    private CartaoService cartaoService;

    @MockBean
    private ClienteService clienteService;

    @Autowired
    private PagamentoService pagamentoService;

    @Test
    public void testarCriar()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setId(1);
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        Pagamento pagamento = new Pagamento();
        pagamento.setId(1);
        pagamento.setCartao(cartao);
        pagamento.setDescricao("coca cola");
        pagamento.setValor(4.99);

        PagamentoEntrada pagamentoDTO = new PagamentoEntrada();
        pagamentoDTO.setValor(4.99);
        pagamentoDTO.setDescricao("coca cola");
        pagamentoDTO.setCartao_id(1);

        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(cartao);
        Mockito.when(pagamentoRepository.save(Mockito.any(Pagamento.class))).thenReturn(pagamento);

        //Execução
        PagamentoSaida pagamentoResultado = pagamentoService.criar(pagamentoDTO);

        //Verificação
        Assertions.assertEquals(pagamento.getValor(), pagamentoResultado.getValor());
    }

    @Test
    public void testarCriarCartaoNaoEncontrado()
    {
        //Preparação
        PagamentoEntrada pagamentoDTO = new PagamentoEntrada();
        pagamentoDTO.setValor(4.99);
        pagamentoDTO.setDescricao("coca cola");
        pagamentoDTO.setCartao_id(1);

        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.criar(pagamentoDTO);});
    }

    @Test
    public void testarCriarComCartaoInativo()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setId(1);
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(false);

        Pagamento pagamento = new Pagamento();
        pagamento.setId(1);
        pagamento.setCartao(cartao);
        pagamento.setDescricao("coca cola");
        pagamento.setValor(4.99);

        PagamentoEntrada pagamentoDTO = new PagamentoEntrada();
        pagamentoDTO.setValor(4.99);
        pagamentoDTO.setDescricao("coca cola");
        pagamentoDTO.setCartao_id(1);

        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(cartao);
        Mockito.when(pagamentoRepository.save(Mockito.any(Pagamento.class))).thenReturn(pagamento);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.criar(pagamentoDTO);});
    }

    @Test
    public void testarBuscarPorId_cartao()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setId(1);
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        Pagamento pagamento = new Pagamento();
        pagamento.setId(1);
        pagamento.setCartao(cartao);
        pagamento.setDescricao("coca cola");
        pagamento.setValor(4.99);

        List<Pagamento> lista = new ArrayList<>();
        lista.add(pagamento);

        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(cartao);
        Mockito.when(pagamentoRepository.findByCartao(Mockito.any(Cartao.class))).thenReturn(lista);

        //Execução
        List<PagamentoSaida> pagamentoResultado = pagamentoService.buscarPorId_cartao(1);

        //Verificação
        Assertions.assertEquals(1, pagamentoResultado.size());
        Assertions.assertEquals(pagamento.getValor(), pagamentoResultado.get(0).getValor());
        Assertions.assertEquals(pagamento.getDescricao(), pagamentoResultado.get(0).getDescricao());
    }

    @Test
    public void testarBuscarPorId_cartaoNaoEncontrandoCartao()
    {
        //Preparação
        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.buscarPorId_cartao(1);});
    }

    @Test
    public void testarBuscarPorIdClienteId_cartao()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setId(1);
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        Pagamento pagamento = new Pagamento();
        pagamento.setId(1);
        pagamento.setCartao(cartao);
        pagamento.setDescricao("coca cola");
        pagamento.setValor(4.99);

        List<Pagamento> lista = new ArrayList<>();
        lista.add(pagamento);

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(cartao);
        Mockito.when(pagamentoRepository.findByCartao(Mockito.any(Cartao.class))).thenReturn(lista);

        //Execução
        List<PagamentoSaida> pagamentoResultado = pagamentoService.buscarPorIdClienteId_cartao(1, 1);

        //Verificação
        Assertions.assertEquals(1, pagamentoResultado.size());
        Assertions.assertEquals(pagamento.getValor(), pagamentoResultado.get(0).getValor());
        Assertions.assertEquals(pagamento.getDescricao(), pagamentoResultado.get(0).getDescricao());
    }

    @Test
    public void testarBuscarPorIdClienteId_cartaoNaoEncontrandoCartao()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.buscarPorIdClienteId_cartao(1, 1);});
    }

    @Test
    public void testarBuscarPorIdClienteId_cartaoNaoEncontrandoCliente()
    {
        //Preparação
        Optional<Cliente> clienteOptional = Optional.empty();
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.buscarPorIdClienteId_cartao(1, 1);});
    }

    @Test
    public void testarPagar()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setId(1);
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        Pagamento pagamento1 = new Pagamento();
        pagamento1.setId(1);
        pagamento1.setCartao(cartao);
        pagamento1.setDescricao("coca cola");
        pagamento1.setValor(5.0);

        Pagamento pagamento2 = new Pagamento();
        pagamento2.setId(2);
        pagamento2.setCartao(cartao);
        pagamento2.setDescricao("coca cola");
        pagamento2.setValor(5.0);

        Pagamento pagamento3 = new Pagamento();
        pagamento3.setId(3);
        pagamento3.setCartao(cartao);
        pagamento3.setDescricao("pizza");
        pagamento3.setValor(20.0);

        List<Pagamento> lista = new ArrayList<>();
        lista.add(pagamento1);
        lista.add(pagamento2);
        lista.add(pagamento3);

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(cartao);
        Mockito.when(pagamentoRepository.findByCartao(Mockito.any(Cartao.class))).thenReturn(lista);

        //Execução
        ValorPago valorPago = pagamentoService.pagar(1, 1);

        //Verificação
        Assertions.assertEquals(30, valorPago.getValorPago());
    }

    @Test
    public void testarPagarCartaoInexistente()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.pagar(1, 1);});
    }

    @Test
    public void testarPagarClienteInexistente()
    {
        //Preparação
        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.pagar(1, 1);});
    }

    @Test
    public void testarExpirar()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Cartao cartao = new Cartao();
        cartao.setId(1);
        cartao.setCliente(cliente);
        cartao.setNumero("123");
        cartao.setAtivo(true);

        CartaoStatusSaida cartaoStatusSaidaDTO = new CartaoStatusSaida();
        cartaoStatusSaidaDTO.setStatus("ok");

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(cartao);

        //Execução
        CartaoStatusSaida StatusEsperado = pagamentoService.expirar(1, 1);

        //Verificação
        Assertions.assertEquals(cartaoStatusSaidaDTO.getStatus(), StatusEsperado.getStatus());
    }

    @Test
    public void testarExpirarCartaoInexistente()
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.expirar(1, 1);});
    }

    @Test
    public void testarExpirarClienteInexistente()
    {
        //Preparação
        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(null);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.expirar(1, 1);});
    }
}
