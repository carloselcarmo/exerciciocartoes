package br.com.itau.ExercicioCartoes.Pagamento.controller;

import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoStatusSaida;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.PagamentoEntrada;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.PagamentoSaida;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.ValorPago;
import br.com.itau.ExercicioCartoes.Pagamento.service.PagamentoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(PagamentoController.class)
public class PagamentoControllerTeste
{
    @MockBean
    private PagamentoService pagamentoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarCriarPagamento() throws Exception
    {
        //Preparação
        PagamentoEntrada pagamentoDTO = new PagamentoEntrada();
        pagamentoDTO.setValor(4.99);
        pagamentoDTO.setDescricao("coca cola");
        pagamentoDTO.setCartao_id(1);

        PagamentoSaida pagamentoSaidaDTO = new PagamentoSaida();
        pagamentoSaidaDTO.setCartao_id(1);
        pagamentoSaidaDTO.setDescricao("coca cola");
        pagamentoSaidaDTO.setId(1);
        pagamentoSaidaDTO.setValor(4.99);

        Mockito.when(pagamentoService.criar(Mockito.any(PagamentoEntrada.class))).thenReturn(pagamentoSaidaDTO);

        ObjectMapper objectMapper = new ObjectMapper();
        String pagamentoJson = objectMapper.writeValueAsString(pagamentoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/pagamento")
                .contentType(MediaType.APPLICATION_JSON).content(pagamentoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo("coca cola")));
    }

    @Test
    public void testarBuscarPorId_cartao() throws Exception
    {
        //Preparação
        PagamentoSaida pagamentoSaidaDTO = new PagamentoSaida();
        pagamentoSaidaDTO.setCartao_id(1);
        pagamentoSaidaDTO.setDescricao("coca cola");
        pagamentoSaidaDTO.setId(1);
        pagamentoSaidaDTO.setValor(4.99);

        List<PagamentoSaida> lista =  new ArrayList<>();
        lista.add(pagamentoSaidaDTO);

        Mockito.when(pagamentoService.buscarPorId_cartao(Mockito.anyInt())).thenReturn(lista);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/pagamentos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarBuscarPorId_cartaoSemResposta() throws Exception
    {
        //Preparação
        Mockito.when(pagamentoService.buscarPorId_cartao(Mockito.anyInt())).thenThrow(RuntimeException.class);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/pagamentos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarBuscarPorIdClienteId_cartao() throws Exception
    {
        //Preparação
        PagamentoSaida pagamentoSaidaDTO = new PagamentoSaida();
        pagamentoSaidaDTO.setCartao_id(1);
        pagamentoSaidaDTO.setDescricao("coca cola");
        pagamentoSaidaDTO.setId(1);
        pagamentoSaidaDTO.setValor(4.99);

        List<PagamentoSaida> lista =  new ArrayList<>();
        lista.add(pagamentoSaidaDTO);

        Mockito.when(pagamentoService.buscarPorIdClienteId_cartao(Mockito.anyInt(), Mockito.anyInt())).thenReturn(lista);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/fatura/1/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarBuscarPorIdClienteId_cartaoSemResposta() throws Exception
    {
        //Preparação
        Mockito.when(pagamentoService.buscarPorIdClienteId_cartao(Mockito.anyInt(), Mockito.anyInt())).thenThrow(RuntimeException.class);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/fatura/1/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarPagar() throws Exception
    {
        //Preparação
        ValorPago valorPagoDTO = new ValorPago();
        valorPagoDTO.setValorPago(10.5);
        valorPagoDTO.setId(1);
        valorPagoDTO.setPagoEm(LocalDate.now());

        Mockito.when(pagamentoService.pagar(Mockito.anyInt(), Mockito.anyInt())).thenReturn(valorPagoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/fatura/1/1/pagar")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarPagarComErro() throws Exception
    {
        //Preparação
        Mockito.when(pagamentoService.pagar(Mockito.anyInt(), Mockito.anyInt())).thenThrow(RuntimeException.class);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/fatura/1/1/pagar")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarExpirar() throws Exception
    {
        //Preparação
        CartaoStatusSaida cartaoStatusSaidaDTO = new CartaoStatusSaida();
        cartaoStatusSaidaDTO.setStatus("ok");

        Mockito.when(pagamentoService.expirar(Mockito.anyInt(), Mockito.anyInt())).thenReturn(cartaoStatusSaidaDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/fatura/1/1/expirar")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarExpirarComErro() throws Exception
    {
        //Preparação
        Mockito.when(pagamentoService.expirar(Mockito.anyInt(), Mockito.anyInt())).thenThrow(RuntimeException.class);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/fatura/1/1/expirar")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
