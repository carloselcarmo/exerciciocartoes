package br.com.itau.ExercicioCartoes.Cliente.controller;

import br.com.itau.ExercicioCartoes.Cliente.model.Cliente;
import br.com.itau.ExercicioCartoes.Cliente.service.ClienteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(ClienteController.class)
public class ClienteControllerTeste
{
    @MockBean
    private ClienteService clienteService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarCriarCliente() throws Exception
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Mockito.when(clienteService.criar(Mockito.any(Cliente.class))).thenReturn(cliente);

        ObjectMapper objectMapper = new ObjectMapper();
        String clienteJson = objectMapper.writeValueAsString(cliente);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/cliente")
                .contentType(MediaType.APPLICATION_JSON).content(clienteJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.equalTo("Jose")));
    }

    @Test
    public void testarBuscarPorId() throws Exception
    {
        //Preparação
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setName("Jose");

        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenReturn(cliente);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/cliente/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void TestarBuscarPorIdSemResposta() throws Exception
    {
        //Preparação
        Mockito.when(clienteService.buscarPorId(Mockito.anyInt())).thenThrow(RuntimeException.class);


        mockMvc.perform(MockMvcRequestBuilders.get("/cliente/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
