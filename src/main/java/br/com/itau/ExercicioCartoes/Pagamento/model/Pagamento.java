package br.com.itau.ExercicioCartoes.Pagamento.model;

import br.com.itau.ExercicioCartoes.Cartao.model.Cartao;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Pagamento
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @NotNull(message =  "Cartão não pode ser nulo")
    private Cartao cartao;

    @NotNull(message =  "Descrição não pode ser nula")
    @NotBlank(message = "Descrição não pode ser vazia")
    private String descricao;

    @NotNull(message =  "Valor não pode ser nulo")
    @DecimalMin(value = "0", message = "Valor deve ser maior ou igual a zero")
    @Digits(integer = 6, fraction = 2, message = "Valor fora do padrão")
    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Pagamento() {
    }
}
