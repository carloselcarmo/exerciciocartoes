package br.com.itau.ExercicioCartoes.Pagamento.service;

import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoStatus;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoStatusSaida;
import br.com.itau.ExercicioCartoes.Cartao.model.Cartao;
import br.com.itau.ExercicioCartoes.Cartao.service.CartaoService;
import br.com.itau.ExercicioCartoes.Cliente.model.Cliente;
import br.com.itau.ExercicioCartoes.Cliente.service.ClienteService;
import br.com.itau.ExercicioCartoes.Cartao.exception.CartaoInativoException;
import br.com.itau.ExercicioCartoes.Cartao.exception.CartaoNaoEncontradoException;
import br.com.itau.ExercicioCartoes.Cliente.exception.ClienteNaoEncontradoException;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.PagamentoEntrada;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.PagamentoSaida;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.ValorPago;
import br.com.itau.ExercicioCartoes.Pagamento.model.Pagamento;
import br.com.itau.ExercicioCartoes.Pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService
{
    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteService clienteService;

    public PagamentoSaida criar(PagamentoEntrada pagamentoDTO)
    {
        Cartao cartao = buscarCartao(pagamentoDTO.getCartao_id());

         if(cartao.getAtivo())
         {
             Pagamento pagamento = new Pagamento();
             pagamento.setCartao(cartao);
             pagamento.setDescricao(pagamentoDTO.getDescricao());
             pagamento.setValor(pagamentoDTO.getValor());

             pagamento = pagamentoRepository.save(pagamento);

             return new PagamentoSaida(pagamento);
         }
         else
         {
             throw new CartaoInativoException();
         }
    }

    public List<PagamentoSaida> buscarPorId_cartao(int id)
    {
        Cartao cartao = buscarCartao(id);

        List<Pagamento> lista = pagamentoRepository.findByCartao(cartao);

        List<PagamentoSaida> extrato = new ArrayList<>();

        for(Pagamento pagamento : lista)
        {
            extrato.add(new PagamentoSaida(pagamento));
        }

        return extrato;
    }

    private Cliente buscarCliente(int idCliente)
    {
        Cliente cliente = clienteService.buscarPorId(idCliente);

        if(cliente == null)
        {
            throw new ClienteNaoEncontradoException();
        }

        return cliente;
    }

    private Cartao buscarCartao(int idCartao)
    {
        Cartao cartao = cartaoService.buscarPorId(idCartao);

        if(cartao == null)
        {
            throw new CartaoNaoEncontradoException();
        }

        return cartao;
    }

    public List<PagamentoSaida> buscarPorIdClienteId_cartao(int id_cartao, int id_cliente)
    {
        Cliente cliente = buscarCliente(id_cliente);

        Cartao cartao = buscarCartao(id_cartao);

        List<Pagamento> lista = pagamentoRepository.findByCartao(cartao);

        List<PagamentoSaida> extrato = new ArrayList<>();

        for(Pagamento pagamento : lista)
        {
            extrato.add(new PagamentoSaida(pagamento));
        }

        return extrato;
    }

    public ValorPago pagar(int id_cartao, int id_cliente)
    {
        Cliente cliente = buscarCliente(id_cliente);

        Cartao cartao = buscarCartao(id_cartao);

        List<Pagamento> lista = pagamentoRepository.findByCartao(cartao);

        BigDecimal valor = BigDecimal.valueOf(0);

        for(Pagamento pagamento : lista)
        {
            valor = valor.add(BigDecimal.valueOf(pagamento.getValor()));
            pagamentoRepository.delete(pagamento);
        }

        ValorPago valorPagoDTO = new ValorPago();
        valorPagoDTO.setId(id_cartao);
        valorPagoDTO.setPagoEm(LocalDate.now());
        valorPagoDTO.setValorPago(valor.setScale(2, RoundingMode.HALF_EVEN).doubleValue());

        return valorPagoDTO;
    }

    public CartaoStatusSaida expirar(int id_cartao, int id_cliente)
    {
        Cliente cliente = buscarCliente(id_cliente);

        Cartao cartao = buscarCartao(id_cartao);

        CartaoStatus cartaoStatusDTO = new CartaoStatus();
        cartaoStatusDTO.setAtivo(false);

        cartaoService.alterarStatusAtivo(cartao.getNumero(), cartaoStatusDTO);

        CartaoStatusSaida cartaoStatusSaidaDTO = new CartaoStatusSaida();
        cartaoStatusSaidaDTO.setStatus("ok");

        return cartaoStatusSaidaDTO;
    }
}
