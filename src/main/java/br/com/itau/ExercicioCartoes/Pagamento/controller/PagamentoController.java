package br.com.itau.ExercicioCartoes.Pagamento.controller;

import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoStatusSaida;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.PagamentoEntrada;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.PagamentoSaida;
import br.com.itau.ExercicioCartoes.Pagamento.DTO.ValorPago;
import br.com.itau.ExercicioCartoes.Pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController
{
    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoSaida criar(@RequestBody @Valid PagamentoEntrada pagamento)
    {
        return  pagamentoService.criar(pagamento);
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public List<PagamentoSaida> buscarPorId_cartao(@PathVariable(name = "id_cartao") int id_cartao)
    {
        try
        {
            List<PagamentoSaida> pagamentos = pagamentoService.buscarPorId_cartao(id_cartao);
            return pagamentos;
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/fatura/{cliente_id}/{cartao_id}")
    public List<PagamentoSaida> buscarPorIdClienteId_cartao(@PathVariable(name = "cartao_id") int id_cartao, @PathVariable(name = "cliente_id") int id_cliente)
    {
        try
        {
            List<PagamentoSaida> pagamentos = pagamentoService.buscarPorIdClienteId_cartao(id_cartao, id_cliente);
            return pagamentos;
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/fatura/{cliente_id}/{cartao_id}/pagar")
    public ValorPago pagar(@PathVariable(name = "cartao_id") int id_cartao, @PathVariable(name = "cliente_id") int id_cliente)
    {
        try
        {
            ValorPago valorPago = pagamentoService.pagar(id_cartao, id_cliente);
            return valorPago;
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/fatura/{cliente_id}/{cartao_id}/expirar")
    public CartaoStatusSaida expirar(@PathVariable(name = "cartao_id") int id_cartao, @PathVariable(name = "cliente_id") int id_cliente)
    {
        try
        {
            CartaoStatusSaida cartaoStatus = pagamentoService.expirar(id_cartao, id_cliente);
            return cartaoStatus;
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
