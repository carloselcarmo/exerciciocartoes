package br.com.itau.ExercicioCartoes.Pagamento.DTO;

import java.time.LocalDate;

public class ValorPago
{
    private Integer id;

    private Double valorPago;

    private LocalDate pagoEm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public LocalDate getPagoEm() {
        return pagoEm;
    }

    public void setPagoEm(LocalDate pagoEm) {
        this.pagoEm = pagoEm;
    }

    public ValorPago() {
    }
}
