package br.com.itau.ExercicioCartoes.Pagamento.repository;
import br.com.itau.ExercicioCartoes.Cartao.model.Cartao;
import br.com.itau.ExercicioCartoes.Pagamento.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento,Integer >
{
    List<Pagamento> findByCartao(Cartao cartao);
}
