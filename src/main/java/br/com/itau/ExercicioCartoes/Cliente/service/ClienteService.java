package br.com.itau.ExercicioCartoes.Cliente.service;

import br.com.itau.ExercicioCartoes.Cliente.exception.ClienteNaoEncontradoException;
import br.com.itau.ExercicioCartoes.Cliente.model.Cliente;
import br.com.itau.ExercicioCartoes.Cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService
{
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criar (Cliente cliente)
    {
        return  clienteRepository.save(cliente);
    }

    public Cliente buscarPorId(int id)
    {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent())
        {
            Cliente cliente = clienteOptional.get();
            return  cliente;
        }
        else
        {
            throw new ClienteNaoEncontradoException ();
        }
    }
}
