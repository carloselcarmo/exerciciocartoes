package br.com.itau.ExercicioCartoes.Cliente.controller;

import br.com.itau.ExercicioCartoes.Cliente.model.Cliente;
import br.com.itau.ExercicioCartoes.Cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController
{
    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criar(@RequestBody @Valid Cliente cliente)
    {
        return  clienteService.criar(cliente);
    }

    @GetMapping("/{id}")
    public Cliente buscarPorId(@PathVariable(name = "id") int id)
    {
        try
        {
            Cliente cliente = clienteService.buscarPorId(id);
            return cliente;
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
