package br.com.itau.ExercicioCartoes.Cliente.repository;

import br.com.itau.ExercicioCartoes.Cliente.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente,Integer > {
}
