package br.com.itau.ExercicioCartoes.Cartao.controller;

import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoEntrada;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoSaida;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoSemStatus;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoStatus;
import br.com.itau.ExercicioCartoes.Cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController
{
    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoSaida criar(@RequestBody @Valid CartaoEntrada cartao)
    {
        return  cartaoService.criar(cartao);
    }

    @PatchMapping("/{numero}")
    public CartaoSaida alterarStatusAtivo(@PathVariable(name = "numero") String numero, @RequestBody @Valid CartaoStatus cartaoStatusDTO)
    {
        try
        {
            return cartaoService.alterarStatusAtivo(numero, cartaoStatusDTO);
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public CartaoSemStatus buscarPorNumero(@PathVariable(name = "numero") String numero)
    {
        try
        {
            return cartaoService.buscarPorNumero(numero);
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
