package br.com.itau.ExercicioCartoes.Cartao.model;

import br.com.itau.ExercicioCartoes.Cliente.model.Cliente;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Cartao
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message =  "Número não pode ser nulo")
    @NotBlank(message = "Número não pode ser vazio")
    private String numero;

    @ManyToOne
    @NotNull(message =  "Cliente não pode ser nulo")
    private Cliente cliente;

    @NotNull(message =  "Ativo não  pode ser nulo")
    private Boolean ativo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Cartao() {
    }
}
