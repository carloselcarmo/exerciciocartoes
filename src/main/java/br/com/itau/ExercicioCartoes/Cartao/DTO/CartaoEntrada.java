package br.com.itau.ExercicioCartoes.Cartao.DTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoEntrada
{
    @NotNull(message =  "Número não pode ser nulo")
    @NotBlank(message = "Número não pode ser vazio")
    private String numero;

    @NotNull(message =  "ClienteId não pode ser nulo")
    private Integer clienteId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public CartaoEntrada() {
    }
}
