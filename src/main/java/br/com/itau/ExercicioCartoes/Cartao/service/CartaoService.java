package br.com.itau.ExercicioCartoes.Cartao.service;

import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoEntrada;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoSaida;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoSemStatus;
import br.com.itau.ExercicioCartoes.Cartao.DTO.CartaoStatus;
import br.com.itau.ExercicioCartoes.Cartao.exception.CartaoNaoEncontradoException;
import br.com.itau.ExercicioCartoes.Cartao.exception.NumeroCartaoJaUtilizadoException;
import br.com.itau.ExercicioCartoes.Cartao.model.Cartao;
import br.com.itau.ExercicioCartoes.Cartao.repository.CartaoRepository;
import br.com.itau.ExercicioCartoes.Cliente.model.Cliente;
import br.com.itau.ExercicioCartoes.Cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService
{
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public CartaoSaida criar(CartaoEntrada cartaoDTO)
    {
        Cliente cliente = clienteService.buscarPorId(cartaoDTO.getClienteId());

        Cartao cartaoDb = cartaoRepository.findByNumero(cartaoDTO.getNumero());

        if(cartaoDb != null)
        {
            throw new NumeroCartaoJaUtilizadoException();
        }
        else
        {
            Cartao cartao = new Cartao();
            cartao.setCliente(cliente);
            cartao.setNumero(cartaoDTO.getNumero());
            cartao.setAtivo(false);

            cartao = cartaoRepository.save(cartao);

            return new CartaoSaida(cartao);
        }
    }

    public Cartao buscarPorId(Integer id)
    {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);

        if(cartaoOptional.isPresent())
        {
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }
        else
        {
            throw new CartaoNaoEncontradoException();
        }
    }

    public CartaoSaida alterarStatusAtivo(String numero, CartaoStatus cartaoStatusDTO)
    {
        Cartao cartao = buscarCartaoPorNumero(numero);

        cartao.setAtivo(cartaoStatusDTO.getAtivo());

        cartao = cartaoRepository.save(cartao);

        return new CartaoSaida(cartao);
    }

    public CartaoSemStatus buscarPorNumero(String numero)
    {
        return new CartaoSemStatus(buscarCartaoPorNumero(numero));
    }

    private Cartao buscarCartaoPorNumero(String numero)
    {
        Cartao cartao = cartaoRepository.findByNumero(numero);

        if(cartao == null)
        {
            throw new CartaoNaoEncontradoException();
        }

        return cartao;
    }
}
