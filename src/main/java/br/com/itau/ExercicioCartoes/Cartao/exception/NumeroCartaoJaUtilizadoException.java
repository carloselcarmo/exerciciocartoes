package br.com.itau.ExercicioCartoes.Cartao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.CONFLICT, reason = "Cartão já utilizado")
public class NumeroCartaoJaUtilizadoException extends RuntimeException {
}
