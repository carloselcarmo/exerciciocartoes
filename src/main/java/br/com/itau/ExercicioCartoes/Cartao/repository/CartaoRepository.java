package br.com.itau.ExercicioCartoes.Cartao.repository;

import br.com.itau.ExercicioCartoes.Cartao.model.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartaoRepository extends JpaRepository<Cartao,Integer >
{
    Cartao findByNumero(String numero);
}
