package br.com.itau.ExercicioCartoes.Cartao.DTO;


import br.com.itau.ExercicioCartoes.Cartao.model.Cartao;

public class CartaoSaida
{
    private Integer id;

    private String numero;

    private Integer clienteId;

    private Boolean ativo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public CartaoSaida() {
    }

    public CartaoSaida(Cartao cartao)
    {
        this.setAtivo(cartao.getAtivo());
        this.setClienteId(cartao.getCliente().getId());
        this.setId(cartao.getId());
        this.setNumero(cartao.getNumero());
    }
}
