package br.com.itau.ExercicioCartoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExercicioCartoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExercicioCartoesApplication.class, args);
	}

}
