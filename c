[1mdiff --git a/src/main/java/br/com/itau/ExercicioCartoes/exceptions/CartaoInativoException.java b/src/main/java/br/com/itau/ExercicioCartoes/exceptions/CartaoInativoException.java[m
[1mindex 5e13d85..54e9c0d 100644[m
[1m--- a/src/main/java/br/com/itau/ExercicioCartoes/exceptions/CartaoInativoException.java[m
[1m+++ b/src/main/java/br/com/itau/ExercicioCartoes/exceptions/CartaoInativoException.java[m
[36m@@ -1,4 +1,8 @@[m
 package br.com.itau.ExercicioCartoes.exceptions;[m
 [m
[32m+[m[32mimport org.springframework.http.HttpStatus;[m
[32m+[m[32mimport org.springframework.web.bind.annotation.ResponseStatus;[m
[32m+[m
[32m+[m[32m@ResponseStatus(code= HttpStatus.FAILED_DEPENDENCY, reason = "Cartão inativo")[m
 public class CartaoInativoException extends RuntimeException  {[m
 }[m
[1mdiff --git a/src/main/java/br/com/itau/ExercicioCartoes/exceptions/CartaoNaoEncontradoException.java b/src/main/java/br/com/itau/ExercicioCartoes/exceptions/CartaoNaoEncontradoException.java[m
[1mindex bb8aecf..55f36d7 100644[m
[1m--- a/src/main/java/br/com/itau/ExercicioCartoes/exceptions/CartaoNaoEncontradoException.java[m
[1m+++ b/src/main/java/br/com/itau/ExercicioCartoes/exceptions/CartaoNaoEncontradoException.java[m
[36m@@ -1,4 +1,8 @@[m
 package br.com.itau.ExercicioCartoes.exceptions;[m
 [m
[32m+[m[32mimport org.springframework.http.HttpStatus;[m
[32m+[m[32mimport org.springframework.web.bind.annotation.ResponseStatus;[m
[32m+[m
[32m+[m[32m@ResponseStatus(code= HttpStatus.NOT_FOUND, reason = "Cartão não encontrado")[m
 public class CartaoNaoEncontradoException extends RuntimeException  {[m
 }[m
[1mdiff --git a/src/main/java/br/com/itau/ExercicioCartoes/exceptions/ClienteNaoEncontradoException.java b/src/main/java/br/com/itau/ExercicioCartoes/exceptions/ClienteNaoEncontradoException.java[m
[1mindex 0b8ed3d..d6eeee1 100644[m
[1m--- a/src/main/java/br/com/itau/ExercicioCartoes/exceptions/ClienteNaoEncontradoException.java[m
[1m+++ b/src/main/java/br/com/itau/ExercicioCartoes/exceptions/ClienteNaoEncontradoException.java[m
[36m@@ -1,4 +1,8 @@[m
 package br.com.itau.ExercicioCartoes.exceptions;[m
 [m
[32m+[m[32mimport org.springframework.http.HttpStatus;[m
[32m+[m[32mimport org.springframework.web.bind.annotation.ResponseStatus;[m
[32m+[m
[32m+[m[32m@ResponseStatus(code= HttpStatus.NOT_FOUND, reason = "Cliente não encontrado")[m
 public class ClienteNaoEncontradoException extends RuntimeException {[m
 }[m
[1mdiff --git a/src/main/java/br/com/itau/ExercicioCartoes/exceptions/NumeroCartaoJaUtilizadoException.java b/src/main/java/br/com/itau/ExercicioCartoes/exceptions/NumeroCartaoJaUtilizadoException.java[m
[1mindex eba5d67..b4061c5 100644[m
[1m--- a/src/main/java/br/com/itau/ExercicioCartoes/exceptions/NumeroCartaoJaUtilizadoException.java[m
[1m+++ b/src/main/java/br/com/itau/ExercicioCartoes/exceptions/NumeroCartaoJaUtilizadoException.java[m
[36m@@ -1,4 +1,8 @@[m
 package br.com.itau.ExercicioCartoes.exceptions;[m
 [m
[32m+[m[32mimport org.springframework.http.HttpStatus;[m
[32m+[m[32mimport org.springframework.web.bind.annotation.ResponseStatus;[m
[32m+[m
[32m+[m[32m@ResponseStatus(code= HttpStatus.CONFLICT, reason = "Cartão já utilizado")[m
 public class NumeroCartaoJaUtilizadoException extends RuntimeException {[m
 }[m
[1mdiff --git a/src/main/java/br/com/itau/ExercicioCartoes/services/CartaoService.java b/src/main/java/br/com/itau/ExercicioCartoes/services/CartaoService.java[m
[1mindex a71dfbe..dd24248 100644[m
[1m--- a/src/main/java/br/com/itau/ExercicioCartoes/services/CartaoService.java[m
[1m+++ b/src/main/java/br/com/itau/ExercicioCartoes/services/CartaoService.java[m
[36m@@ -28,29 +28,22 @@[m [mpublic class CartaoService[m
     {[m
         Cliente cliente = clienteService.buscarPorId(cartaoDTO.getClienteId());[m
 [m
[31m-        if(cliente != null)[m
[32m+[m[32m        Cartao cartaoDb = cartaoRepository.findByNumero(cartaoDTO.getNumero());[m
[32m+[m
[32m+[m[32m        if(cartaoDb != null)[m
         {[m
[31m-            Cartao cartaoDb = cartaoRepository.findByNumero(cartaoDTO.getNumero());[m
[31m-[m
[31m-            if(cartaoDb != null)[m
[31m-            {[m
[31m-                throw new NumeroCartaoJaUtilizadoException();[m
[31m-            }[m
[31m-            else[m
[31m-            {[m
[31m-                Cartao cartao = new Cartao();[m
[31m-                cartao.setCliente(cliente);[m
[31m-                cartao.setNumero(cartaoDTO.getNumero());[m
[31m-                cartao.setAtivo(false);[m
[31m-[m
[31m-                cartao = cartaoRepository.save(cartao);[m
[31m-[m
[31m-                return new CartaoSaidaDTO(cartao);[m
[31m-            }[m
[32m+[m[32m            throw new NumeroCartaoJaUtilizadoException();[m
         }[m
         else[m
         {[m
[31m-            throw new ClienteNaoEncontradoException();[m
[32m+[m[32m            Cartao cartao = new Cartao();[m
[32m+[m[32m            cartao.setCliente(cliente);[m
[32m+[m[32m            cartao.setNumero(cartaoDTO.getNumero());[m
[32m+[m[32m            cartao.setAtivo(false);[m
[32m+[m
[32m+[m[32m            cartao = cartaoRepository.save(cartao);[m
[32m+[m
[32m+[m[32m            return new CartaoSaidaDTO(cartao);[m
         }[m
     }[m
 [m
[36m@@ -71,33 +64,29 @@[m [mpublic class CartaoService[m
 [m
     public CartaoSaidaDTO alterarStatusAtivo(String numero, CartaoStatusDTO cartaoStatusDTO)[m
     {[m
[31m-        Cartao cartao = cartaoRepository.findByNumero(numero);[m
[32m+[m[32m        Cartao cartao = buscarCartaoPorNumero(numero);[m
 [m
[31m-        if(cartao != null)[m
[31m-        {[m
[31m-            cartao.setAtivo(cartaoStatusDTO.getAtivo());[m
[32m+[m[32m        cartao.setAtivo(cartaoStatusDTO.getAtivo());[m
 [m
[31m-            cartao = cartaoRepository.save(cartao);[m
[32m+[m[32m        cartao = cartaoRepository.save(cartao);[m
 [m
[31m-            return new CartaoSaidaDTO(cartao);[m
[31m-        }[m
[31m-        else[m
[31m-        {[m
[31m-            throw new CartaoNaoEncontradoException();[m
[31m-        }[m
[32m+[m[32m        return new CartaoSaidaDTO(cartao);[m
     }[m
 [m
     public CartaoSemStatusDTO buscarPorNumero(String numero)[m
[32m+[m[32m    {[m
[32m+[m[32m        return new CartaoSemStatusDTO(buscarCartaoPorNumero(numero));[m
[32m+[m[32m    }[m
[32m+[m
[32m+[m[32m    private Cartao buscarCartaoPorNumero(String numero)[m
     {[m
         Cartao cartao = cartaoRepository.findByNumero(numero);[m
 [m
[31m-        if(cartao != null)[m
[31m-        {[m
[31m-            return new CartaoSemStatusDTO(cartao);[m
[31m-        }[m
[31m-        else[m
[32m+[m[32m        if(cartao == null)[m
         {[m
             throw new CartaoNaoEncontradoException();[m
         }[m
[32m+[m
[32m+[m[32m        return cartao;[m
     }[m
 }[m
[1mdiff --git a/src/test/java/br/com/itau/ExercicioCartoes/services/PagamentoServiceTeste.java b/src/test/java/br/com/itau/ExercicioCartoes/services/PagamentoServiceTeste.java[m
[1mindex 69f61f7..1a75e51 100644[m
[1m--- a/src/test/java/br/com/itau/ExercicioCartoes/services/PagamentoServiceTeste.java[m
[1m+++ b/src/test/java/br/com/itau/ExercicioCartoes/services/PagamentoServiceTeste.java[m
[36m@@ -4,6 +4,7 @@[m [mimport br.com.itau.ExercicioCartoes.DTOs.*;[m
 import br.com.itau.ExercicioCartoes.models.Cartao;[m
 import br.com.itau.ExercicioCartoes.models.Cliente;[m
 import br.com.itau.ExercicioCartoes.models.Pagamento;[m
[32m+[m[32mimport br.com.itau.ExercicioCartoes.repositories.ClienteRepository;[m
 import br.com.itau.ExercicioCartoes.repositories.PagamentoRepository;[m
 import org.junit.jupiter.api.Assertions;[m
 import org.junit.jupiter.api.Test;[m
[36m@@ -14,6 +15,7 @@[m [mimport org.springframework.boot.test.mock.mockito.MockBean;[m
 [m
 import java.util.ArrayList;[m
 import java.util.List;[m
[32m+[m[32mimport java.util.Optional;[m
 [m
 @SpringBootTest[m
 public class PagamentoServiceTeste[m
[36m@@ -21,6 +23,9 @@[m [mpublic class PagamentoServiceTeste[m
     @MockBean[m
     private PagamentoRepository pagamentoRepository;[m
 [m
[32m+[m[32m    @MockBean[m
[32m+[m[32m    private ClienteRepository clienteRepository;[m
[32m+[m
     @MockBean[m
     private CartaoService cartaoService;[m
 [m
[36m@@ -212,7 +217,8 @@[m [mpublic class PagamentoServiceTeste[m
     public void testarBuscarPorIdClienteId_cartaoNaoEncontrandoCliente()[m
     {[m
         //Preparação[m
[31m-        Mockito.when(cartaoService.buscarPorId(Mockito.anyInt())).thenReturn(null);[m
[32m+[m[32m        Optional<Cliente> clienteOptional = Optional.empty();[m
[32m+[m[32m        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);[m
 [m
         //Execução + Verificação[m
         Assertions.assertThrows(RuntimeException.class, () -> {pagamentoService.buscarPorIdClienteId_cartao(1, 1);});[m
